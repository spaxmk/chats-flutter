import 'package:flutter/material.dart';

class CategorySelector extends StatefulWidget {
  const CategorySelector({Key? key}) : super(key: key);

  @override
  _CategorySelectorState createState() => _CategorySelectorState();
}

class _CategorySelectorState extends State<CategorySelector> {
  int selectedIndex = 0;
  final List<String> categories = ['Message', 'Online', 'Groups', 'Requests'];


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90.0,
      color: Colors.blueGrey,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (BuildContext context, int index){
          return GestureDetector(
            onTap: (){
              setState(() {
                selectedIndex = index;
              });
            },
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 30.0
                ),
                child: Text(
                    categories[index],
                    style: TextStyle(
                        color: index == selectedIndex ? Colors.white : Colors.white60,
                        fontSize: 24.0,
                        fontWeight: FontWeight.normal,
                        letterSpacing: 1.2
                    )
                ),
              )
          );


        },
      ),
    );
  }
}
