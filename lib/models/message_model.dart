import 'package:chat_ui/models/user_model.dart';

class Message{
  final User sender;
  final String time;
  final String text;
  final bool isLiked;
  final bool unread;

  Message(this.sender, this.time, this.text, this.isLiked, this.unread);

}

  //Curent user
  final User currentUser = User(id: 0, name: 'Current User', imageUrl: 'assets/images/1.jpg');

  //Users
  final User jovana = User(
      id: 1,
      name: 'Jovana',
      imageUrl: 'assets/images/2.jpg');

  final User ana = User(id: 2, name: 'Ana', imageUrl: 'assets/images/3.jpg');
  final User marija = User(id: 3, name: 'Marija', imageUrl: 'assets/images/4.jpg');
  final User tamara = User(id: 4, name: 'Tamara', imageUrl: 'assets/images/5.jpg');
  final User anastasia = User(id: 5, name: 'Anastasia', imageUrl: 'assets/images/6.jpg');

  //Favorite contacts
  List<User> favorites = [jovana, ana, anastasia, marija, tamara];

  //Example chats on home screen
  List<Message> chats = [
    Message(jovana, '5:30 PM', 'Text messaage 7', false, true),
    Message(ana, '5:40 PM', 'Text messaage 8', false, true),
    Message(marija, '5:45 PM', 'Text messaage 9', true, false),
    Message(tamara, '6:30 PM', 'Text messaage 10', false, false),
    Message(jovana, '7:10 PM', 'Text messaage 11', true, true),
    Message(anastasia, '8:20 PM', 'Text messaage 12', false, false),
    Message(anastasia, '8:20 PM', 'Text messaage 12', false, true),
    Message(anastasia, '8:20 PM', 'Text messaage 12', false, false),
    Message(anastasia, '8:20 PM', 'Text messaage 12', false, true),
    Message(anastasia, '8:20 PM', 'Text messaage 12', false, false),


  ];

  //Example messages in chat screen
  List<Message> messages = [
    Message(jovana, '5:30 PM', 'Text messaage1', false, true),
    Message(ana, '5:40 PM', 'Text messaage 2', false, false),
    Message(marija, '5:45 PM', 'Text messaage 3', true, true),
    Message(anastasia, '6:30 PM', 'Text messaage 4', false, true),
    Message(tamara, '7:10 PM', 'Text messaage 5', true, true),
    Message(jovana, '8:20 PM', 'Text messaage 6', false, false),
    Message(jovana, '8:20 PM', 'Text messaage 6', false, false),
    Message(jovana, '8:20 PM', 'Text messaage 6', false, false),
    Message(jovana, '8:20 PM', 'Text messaage 6', false, false),
  ];

